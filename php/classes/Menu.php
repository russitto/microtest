<?php

class Menu
{
    private static $instance = null;
    public $db;

    public function __construct()
    {
        header("Content-Type: application/json");
        $this->db = new DB\SQL(
            "mysql:host=devel;port=3306;dbname=qubitv4",
            "qubit",
            "qubit"
        );
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public static function getAll()
    {
        $inst = self::getInstance();
        $menues = $inst->db->exec("SELECT * FROM menu");
        echo json_encode($menues);
    }
}
