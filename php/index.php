<?php
require "vendor/autoload.php";
$f3 = \Base::instance() ;
$f3->set('AUTOLOAD', 'classes/');

$f3->route('GET /', function($f3) {
    echo 'Hola mundo';
});

$f3->route('GET /menu', 'Menu::getAll');
$f3->route('POST /menu', 'Menu::create');
$f3->map('menu/@id', 'Menu');

$f3->run();
