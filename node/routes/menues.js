var express = require('express');
var router = express.Router();
var request = require('request');
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'devel',
  user     : 'qubit',
  password : 'qubit',
  database : 'qubitv4'
});
 
connection.connect();

/* GET menues listing. */
router.get('/', function(req, res, next) {
  res.set('Content-Type', 'application/json');
  request({json:true, uri:'http://localhost/menu/'}, function (err, response, body) {
     var criterias = [];
     for (var i = 0, len = body.length; i < len; i++) {
         var el = body[i];
         if (el.named_criteria !=  '') {
             if (criterias.indexOf(el.named_criteria) == -1) {
                criterias.push(el.named_criteria);
             }
         }
     }
     connection.query('SELECT * FROM search_criteria WHERE name IN ("' + criterias.join('","') + '")', function (err, rows, fields) {
         if (err) console.log(err);
         res.send(JSON.stringify(rows));
     });
  });
});

module.exports = router;
