package main

import (
    "github.com/ant0ine/go-json-rest/rest"
    _ "github.com/go-sql-driver/mysql"
    "github.com/jinzhu/gorm"
    "log"
    "net/http"
//    "time"
)

func main() {

    i := Impl{}
    i.InitDB()
    i.InitSchema()

    api := rest.NewApi()
    api.Use(rest.DefaultDevStack...)
    router, err := rest.MakeRouter(
        rest.Get("/menues", i.GetAllMenues),
        // rest.Post("/menues", i.PostReminder),
        // rest.Get("/menues/:id", i.GetReminder),
        // rest.Put("/menues/:id", i.PutReminder),
        // rest.Delete("/menues/:id", i.DeleteReminder),
    )
    if err != nil {
        log.Fatal(err)
    }
    api.SetApp(router)
    log.Fatal(http.ListenAndServe(":8080", api.MakeHandler()))
}

type Menu struct {
    Id            int64     `json:"id"`
    Text          string    `sql:"size:100" json:"text"`
    DeviceId      int       `json:"device_id"`
    NamedCriteria string    `sql:"size:50" json:"named_criteria"`
    ParentId      int       `json:"parent_id"`
    Order         int       `json:"order"`
    Icon          string    `sql:"size:255" json:"icon"`
    Url           string    `sql:"size:255" json:"url"`
    Status        string    `sql:"size:100" json:"status"`
    UserLogged    bool      `json:"user_logged"`
}

type Impl struct {
    DB gorm.DB
}

func (i *Impl) InitDB() {
    var err error
    i.DB, err = gorm.Open("mysql", "qubit:qubit@tcp(devel:3306)/qubitv4?charset=utf8&parseTime=True")
    if err != nil {
        log.Fatalf("Got error when connect database, the error is '%v'", err)
    }
    i.DB.LogMode(true)
    i.DB.SingularTable(true)
}

func (i *Impl) InitSchema() {
    //i.DB.AutoMigrate(&Menu{})
}

func (i *Impl) GetAllMenues(w rest.ResponseWriter, r *rest.Request) {
    menues := []Menu{}
    i.DB.Find(&menues)
    w.WriteJson(&menues)
}
