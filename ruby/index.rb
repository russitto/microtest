require 'sinatra'
# sinatra/json in sinatra-contrib
require 'sinatra/json'
require 'active_record'


ActiveRecord::Base.establish_connection(
  :adapter  => "mysql2",
  :host     => "devel",
  :username => "qubit",
  :password => "qubit",
  :database => "qubitv4"
)

class Menu < ActiveRecord::Base
    self.table_name = "menu"
end

get '/' do
    json :foo => 'bar'
end

get '/hi' do
    "Hello World!"
end

get '/menues' do
    @menues = Menu.all
    json :menues => @menues
end
