from flask import Flask
from flask.ext.mysql import MySQL
from flask import jsonify
app = Flask(__name__)
mysql = MySQL()

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'qubit'
app.config['MYSQL_DATABASE_PASSWORD'] = 'qubit'
app.config['MYSQL_DATABASE_DB'] = 'qubitv4'
app.config['MYSQL_DATABASE_HOST'] = 'devel'
mysql.init_app(app)
cursor = mysql.connect().cursor()

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/criterias")
def criterias():
    cursor.execute("SELECT * FROM featured_criteria")
    data = cursor.fetchall()
    return jsonify(criterias=data)

if __name__ == "__main__":
    app.debug = True
    app.run()
